/*
 * Copyright (C) 2019 Marius Gripsgard <marius@ubports.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

const fs = require("fs");

const requiredConfigs = [
    {conf: "spaces", "confs": ["accessKeyId", "secretAccessKey", "endpoint"]},
    {conf: "database", "confs": ["uri"]}
]

const requiredEnv = ["CONFIG_DATABASE_URI", "CONFIG_AUTH_KEYS",
                     "CONFIG_SPACES_ACCESSKEYID", "CONFIG_SPACES_SECRETACCESSKEY",
                     "CONFIG_SPACES_ENDPOINT"];


// Check required config
const checkForMissingConfig = (confs, configs) => {
    for (var i = 0; i < confs.length; i++) {
      var conf = confs[i];
      if (typeof conf === "string") {
        if (!configs[conf]) {
          console.log("Missing config: " + conf);
          process.exit();
          return;
        }
        continue;
      }
      if (!configs[conf.conf]) {
        console.log("Missing upper config: " + conf.conf);
        process.exit();
        return;
      }
      checkForMissingConfig(conf.confs, configs[conf.conf])
    }
}

const checkForMissingEnv = () => {
    requiredEnv.forEach((env) => {
        if (!process.env[env]) {
            console.log("Missing config enviroment varable: " + env);
            process.exit();
            return;
        }
    });
}

function configFile() {
    if (fs.existsSync(__dirname+"/../config.json")) {
        let conf = require(__dirname+"/../config.json");
        checkForMissingConfig(requiredConfigs, conf);
        return conf;
    }
    return false;
}

function database() {
    const conf = configFile();
    if (conf) {
        console.log("using configfile for database");
        return conf.database;
    }

    checkForMissingEnv();
    console.log("using envar for database");
    return {
        uri: process.env.CONFIG_DATABASE_URI
    };
}

function auth() {
    const conf = configFile();
    if (conf) {
        console.log("using configfile for auth");
        return conf.auth;
    }

    checkForMissingEnv();
    console.log("using envar for auth");
    return {
        keys: process.env.CONFIG_AUTH_KEYS
    };
}

function spaces() {
    const conf = configFile();
    if (conf) {
        console.log("using configfile for spaces");
        return conf.spaces;
    }

    checkForMissingEnv();
    console.log("using envar for spaces");
    return {
        accessKeyId: process.env.CONFIG_SPACES_ACCESSKEYID,
        secretAccessKey: process.env.CONFIG_SPACES_SECRETACCESSKEY,
        endpoint: process.env.CONFIG_SPACES_ENDPOINT
    }
}

module.exports = {
    database: database,
    auth: auth,
    spaces: spaces
}